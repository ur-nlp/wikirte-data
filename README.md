## WikiRTE dataset processor

This repository houses code used to extract articles
from a Wikipedia data dump [TODO:cite] and process
them to find out claims and supporting evidence
using citation marks in the articles.

{WIP}
The code includes documentation that describes
the method, inclusion/exclusion criteria, and
ways to run individual functions as a library.

The extracted data is meant to serve as a dataset
for RTE (Recognizing textual entailment) using
sentences from Wikipedia and their corresponding
cited scientific article texts.

#### Relevant files in this repository:

- `crawlprime.py`: this is the main script that
  processes each individual page in the dump (and
  can optionally be called separately on a page from
  memory).
- `download_references.py`: download references data once `crawlprime` has done its job
- `latex/*` {WIP}: the paper describing the dataset,
  method, and stats about the data.
- `xml_pages`: sample XML forms of certain Wikipedia
  articles used to develop and test the script.
- `out`: `crawlprime` generates output of
  individual articles in this directory.
  `{id}.json`: the output generated for article with
  unique ID `id`.
  An example file is included in the repo.
- `refdata/`: `download_references` would generate output in this directory.

#### Usage:
Expects `wiki.xml` (or similar) in the root of the repo.

To extract claims data from the wiki dump, do:
```bash
$   python3 crawlprime.py [-p|--parallelism NUM_WORKERS]
                          [-o|--overwrite TRUE|FALSE]
```
where
- `parallelism`: number of concurrent processes to initiate
- `overwrite`: whether to overwrite previously extracted data, if any

To download evidence text using extracted data, do:
```bash
$   python3 download_references.py [-p|--parallelism NUM_WORKERS]
                                   [-o|--overwrite TRUE|FALSE]
                                   [-nd|--no-download TRUE|FALSE]
```
where
- `no-download`: assume that all references were previously
downloaded and do not try to download missing references
because they were probably bad links or bad evidence

To build a pdf binary of the paper,
do:

    $   make xelatexbib FILE=wikirte-data
or otherwise use your preferred `latexmk` way with `bibtex`.
#### Dependencies:

To install all the dependencies, do

    $   python3 -m pip install -U -r requirements.txt

A summary list of salient dependencies:
- `python3` (environment)
- `nltk` (for tokenizing sentences)
- `spacy` (for POS tagging)
- `mwparserfromhell` (a MediaWiki markup parser)
- `mediawiki-utilities` (tools to deal with Wiki XML dumps)
- `lxml` (parsing xml pages)
- `pdfminer` (to extract pdf text)
<!-- - `numpy` (good to have) -->
