{
    "id": 39,
    "revision": 811448184,
    "sentences": [
        {
            "text": "The proportion reflected is not only determined by properties of the surface itself, but also by the spectral and angular distribution of solar radiation reaching the Earth's surface.",
            "context": "Albedo ( ) is a measure for reflectance or optical brightness ( Latin albedo, whiteness ) of a surface. It is dimensionless and measured on a scale from zero (corresponding to a black body that absorbs all incident radiation) to one (corresponding to a body that reflects all incident radiation).Surface albedo is defined as the ratio of irradiance reflected to the irradiance received by a surface.",
            "refs": [
                {
                    "index": 183,
                    "links": [
                        "http://curry.eas.gatech.edu/Courses/6140/ency/Chapter9/Ency_Atmos/Reflectance_Albedo_Surface.pdf"
                    ]
                }
            ]
        },
        {
            "text": "Most land areas are in an albedo range of 0.1 to 0.4.",
            "context": "The term albedo was introduced into optics by Johann Heinrich Lambert in his 1760 work Photometria. Any albedo in visible light falls within a range of about 0.9 for fresh snow to about 0.04 for charcoal, one of the darkest substances. Deeply shadowed cavities can achieve an effective albedo approaching the zero of a black body. When seen from a distance, the ocean surface has a low albedo, as do most forests, whereas desert areas have some of the highest albedos among landforms.",
            "refs": [
                {
                    "index": 53,
                    "links": [
                        "http://scienceworld.wolfram.com/physics/Albedo.html"
                    ]
                }
            ]
        },
        {
            "text": "Another notable high-albedo body is Eris, with an albedo of 0.96.",
            "context": "The albedos of planet s, satellites and minor planet s such as asteroid s can be used to infer much about their properties. The study of albedos, their dependence on wavelength, lighting angle ( phase angle ), and variation in time comprises a major part of the astronomical field of photometry. For small and far objects that cannot be resolved by telescopes, much of what we know comes from the study of their albedos. For example, the absolute albedo can indicate the surface ice content of outer Solar System objects, the variation of albedo with phase angle gives information about regolith properties, whereas unusually high radar albedo is indicative of high metal content in asteroid s. Enceladus, a moon of Saturn, has one of the highest known albedos of any body in the Solar System, with 99% of EM radiation reflected.",
            "refs": [
                {
                    "index": 65,
                    "links": [
                        "http://meetingorganizer.copernicus.org/EPSC-DPS2011/EPSC-DPS2011-137-8.pdf"
                    ]
                }
            ]
        },
        {
            "text": "Many small objects in the outer Solar System and asteroid belt have low albedos down to about 0.05.",
            "context": "The study of albedos, their dependence on wavelength, lighting angle ( phase angle ), and variation in time comprises a major part of the astronomical field of photometry. For small and far objects that cannot be resolved by telescopes, much of what we know comes from the study of their albedos. For example, the absolute albedo can indicate the surface ice content of outer Solar System objects, the variation of albedo with phase angle gives information about regolith properties, whereas unusually high radar albedo is indicative of high metal content in asteroid s. Enceladus, a moon of Saturn, has one of the highest known albedos of any body in the Solar System, with 99% of EM radiation reflected. Another notable high-albedo body is Eris, with an albedo of 0.96.",
            "refs": [
                {
                    "index": 44,
                    "links": [
                        "http://www.johnstonsarchive.net/astro/tnodiam.html",
                        "https://web.archive.org/web/20081022223827/http://www.johnstonsarchive.net/astro/tnodiam.html"
                    ]
                },
                {
                    "index": 99,
                    "links": [
                        "http://www.johnstonsarchive.net/astro/astalbedo.html",
                        "https://web.archive.org/web/20080517100307/http://www.johnstonsarchive.net/astro/astalbedo.html"
                    ]
                }
            ]
        },
        {
            "text": "A typical comet nucleus has an albedo of 0.04.",
            "context": "For small and far objects that cannot be resolved by telescopes, much of what we know comes from the study of their albedos. For example, the absolute albedo can indicate the surface ice content of outer Solar System objects, the variation of albedo with phase angle gives information about regolith properties, whereas unusually high radar albedo is indicative of high metal content in asteroid s. Enceladus, a moon of Saturn, has one of the highest known albedos of any body in the Solar System, with 99% of EM radiation reflected. Another notable high-albedo body is Eris, with an albedo of 0.96. Many small objects in the outer Solar System and asteroid belt have low albedos down to about 0.05.",
            "refs": [
                {
                    "index": 46,
                    "links": [
                        "http://www.space.com/scienceastronomy/solarsystem/borrelly_dark_011129.html",
                        "https://web.archive.org/web/20090122074028/http://www.space.com/scienceastronomy/solarsystem/borrelly_dark_011129.html"
                    ]
                }
            ]
        },
        {
            "text": "This feedback loop results in a reduced albedo effect.",
            "context": "Tropical and sub-tropical rainforest areas have low albedo, and are much hotter than their temperate forest counterparts, which have lower insolation. Because insolation plays such a big role in the heating and cooling effects of albedo, high insolation areas like the tropics will tend to show a more pronounced fluctuation in local temperature when local albedo changes. Arctic regions notably release more heat back into space than what they absorb, effectively cooling the Earth. This has been a concern since arctic ice and snow has been melting at higher rates due to higher temperatures, creating regions in the arctic that are notably darker (being water or ground which is darker color) and reflects less heat back into space.",
            "refs": [
                {
                    "index": 54,
                    "links": [
                        "https://www.economist.com/news/briefing/21721364-commercial-opportunities-are-vastly-outweighed-damage-climate-thawing-arctic"
                    ]
                }
            ]
        },
        {
            "text": "Albedo affects climate by determining how much radiation a planet absorbs.",
            "context": "Because insolation plays such a big role in the heating and cooling effects of albedo, high insolation areas like the tropics will tend to show a more pronounced fluctuation in local temperature when local albedo changes. Arctic regions notably release more heat back into space than what they absorb, effectively cooling the Earth. This has been a concern since arctic ice and snow has been melting at higher rates due to higher temperatures, creating regions in the arctic that are notably darker (being water or ground which is darker color) and reflects less heat back into space. This feedback loop results in a reduced albedo effect.",
            "refs": [
                {
                    "index": 74,
                    "links": [
                        "https://books.google.com/books?id=av7q4N8Ib6sC&pg=PA53&dq=Albedo+affects+climate+by+determining+how+much+radiation+a+planet+absorbs&hl=en&sa=X&ved=0ahUKEwiawueziOHUAhUN5WMKHVHHCjMQ6AEIJjAA#v=onepage&q=Albedo%20affects%20climate%20by%20determining%20how%20much%20radiation%20a%20planet%20absorbs&f=false"
                    ]
                }
            ]
        },
        {
            "text": "Cryoconite, powdery windblown dust containing soot, sometimes reduces albedo on glaciers and ice sheets.",
            "context": "Over Antarctica they average a little more than 0.8. If a marginally snow-covered area warms, snow tends to melt, lowering the albedo, and hence leading to more snowmelt because more radiation is being absorbed by the snowpack (the ice\u2013albedo positive feedback ). The albedo of freshly fallen snow is far higher than that of the sea water when the ice caps melt due to rising temperatures, it exposes the sea water which absorbs the energy rather than reflect it. This, in turn, causes the sea water to heat up increasing the melting of sea ice which is another example of a positive feedback.",
            "refs": [
                {
                    "index": 104,
                    "links": [
                        "http://ngm.nationalgeographic.com/2010/06/melt-zone/jenkins-text/3"
                    ]
                }
            ]
        },
        {
            "text": "In sunlight, dark clothes absorb more heat and light-coloured clothes reflect it better, thus allowing some control over body temperature by exploiting the albedo effect of the colour of external clothing.",
            "context": "This, in turn, causes the sea water to heat up increasing the melting of sea ice which is another example of a positive feedback. Cryoconite, powdery windblown dust containing soot, sometimes reduces albedo on glaciers and ice sheets. Hence, small errors in albedo can lead to large errors in energy estimates, which is why it is important to measure the albedo of snow-covered areas through remote sensing techniques rather than applying a single value over broad regions. Albedo works on a smaller scale, too.",
            "refs": [
                {
                    "index": 205,
                    "links": [
                        "http://www.ranknfile-ue.org/h&s0897.html"
                    ]
                }
            ]
        },
        {
            "text": "Research showed impacts of over 10%.",
            "context": "Hence, small errors in albedo can lead to large errors in energy estimates, which is why it is important to measure the albedo of snow-covered areas through remote sensing techniques rather than applying a single value over broad regions. Albedo works on a smaller scale, too. In sunlight, dark clothes absorb more heat and light-coloured clothes reflect it better, thus allowing some control over body temperature by exploiting the albedo effect of the colour of external clothing. Albedo can affect the electrical energy output of solar photovoltaic device s. For example, the effects of a spectrally responsive albedo are illustrated by the differences between the spectrally weighted albedo of solar photovoltaic technology based on hydrogenated amorphous silicon (a-Si:H) and crystalline silicon (c-Si)-based compared to traditional spectral-integrated albedo predictions.",
            "refs": [
                {
                    "index": 36,
                    "links": [
                        "https://www.academia.edu/3081684"
                    ]
                }
            ]
        },
        {
            "text": "More recently, the analysis was extended to the effects of spectral bias due to the specular reflectivity of 22 commonly occurring surface materials (both human-made and natural) and analyzes the albedo effects on the performance of seven photovoltaic materials covering three common photovoltaic system topologies: industrial (solar farms), commercial flat rooftops and residential pitched-roof applications.",
            "context": "Albedo works on a smaller scale, too. In sunlight, dark clothes absorb more heat and light-coloured clothes reflect it better, thus allowing some control over body temperature by exploiting the albedo effect of the colour of external clothing. Albedo can affect the electrical energy output of solar photovoltaic device s. For example, the effects of a spectrally responsive albedo are illustrated by the differences between the spectrally weighted albedo of solar photovoltaic technology based on hydrogenated amorphous silicon (a-Si:H) and crystalline silicon (c-Si)-based compared to traditional spectral-integrated albedo predictions. Research showed impacts of over 10%.",
            "refs": [
                {
                    "index": 409,
                    "links": [
                        "https://www.academia.edu/6222506"
                    ]
                }
            ]
        },
        {
            "text": "The water vapor causes cooling on the land surface, causes heating where it condenses, acts a strong greenhouse gas, and can increase albedo when it condenses into clouds Scientists generally treat evapotranspiration as a net cooling impact, and the net climate impact of albedo and evapotranspiration changes from deforestation depends greatly on local climate In seasonally snow-covered zones, winter albedos of treeless areas are 10% to 50% higher than nearby forested areas because snow does not cover the trees as readily.",
            "context": "More recently, the analysis was extended to the effects of spectral bias due to the specular reflectivity of 22 commonly occurring surface materials (both human-made and natural) and analyzes the albedo effects on the performance of seven photovoltaic materials covering three common photovoltaic system topologies: industrial (solar farms), commercial flat rooftops and residential pitched-roof applications. Because forests generally have a low albedo, (the majority of the ultraviolet and visible spectrum is absorbed through photosynthesis ), some scientists have suggested that greater heat absorption by trees could offset some of the carbon benefits of afforestation (or offset the negative climate impacts of deforestation ). In the case of evergreen forests with seasonal snow cover albedo reduction may be great enough for deforestation to cause a net cooling effect. Trees also impact climate in extremely complicated ways through evapotranspiration.",
            "refs": [
                {
                    "index": 170,
                    "links": [
                        "https://www.academia.edu/25329256"
                    ]
                }
            ]
        },
        {
            "text": "A study following the burning of the Kuwaiti oil fields during Iraqi occupation showed that temperatures under the burning oil fires were as much as 10 \u00b0C colder than temperatures several miles away under clear skies.",
            "context": "Different types of clouds exhibit different reflectivity, theoretically ranging in albedo from a minimum of near 0 to a maximum approaching 0.8. On any given day, about half of Earth is covered by clouds, which reflect more sunlight than land and water. Clouds keep Earth cool by reflecting sunlight, but they can also serve as blankets to trap warmth. Albedo and climate in some areas are affected by artificial clouds, such as those created by the contrail s of heavy commercial airliner traffic.",
            "refs": [
                {
                    "index": 217,
                    "links": [
                        "https://www.researchgate.net/publication/23842551"
                    ]
                }
            ]
        },
        {
            "text": "The direct (albedo) effect is generally to cool the planet; the indirect effect (the particles act as cloud condensation nuclei and thereby change cloud properties) is less certain.",
            "context": "Clouds keep Earth cool by reflecting sunlight, but they can also serve as blankets to trap warmth. Albedo and climate in some areas are affected by artificial clouds, such as those created by the contrail s of heavy commercial airliner traffic. A study following the burning of the Kuwaiti oil fields during Iraqi occupation showed that temperatures under the burning oil fires were as much as 10 \u00b0C colder than temperatures several miles away under clear skies. Aerosols (very fine particles/droplets in the atmosphere) have both direct and indirect effects on Earth's radiative balance.",
            "refs": [
                {
                    "index": 181,
                    "links": [
                        "http://www.grida.no/climate/ipcc_tar/wg1/231.htm#671",
                        "https://web.archive.org/web/20110629175429/http://www.grida.no/climate/ipcc_tar/wg1/231.htm"
                    ]
                }
            ]
        }
    ]
}