#!/usr/bin/env python3


import nltk
import spacy
from spacy.tokenizer import Tokenizer

nlp = spacy.load('en_core_web_sm')
tokenizer = Tokenizer(nlp.vocab)


def negate_sentence(s):
    '''
    takes as input a sentence s and returns the
    negated form of it, if possible, according to
    a rule-based algorithm given in
    https://www.aclweb.org/anthology/W15-0511
    otherwise, returns None
    '''

    tokens = tokenizer(s)
    #


'''
In [26]: [(t, t.tag_) for t in doc]
Out[26]: 
[(the, 'DT'),
 (birds, 'NNS'),
 (fly, 'VBP'),
 (across, 'IN'),
 (the, 'DT'),
 (ocean, 'NN')]

In [27]: [(t, t.tag_) for t in nlp('I cannot do this')]
Out[27]: [(I, 'PRP'), (can, 'MD'), (not, 'RB'), (do, 'VB'), (this, 'DT')]

In [28]: [(t, t.tag_) for t in nlp('I can't do this')]
  File "<ipython-input-28-2731919d7495>", line 1
    [(t, t.tag_) for t in nlp('I can't do this')]
                                     ^
SyntaxError: invalid syntax


In [29]: [(t, t.tag_) for t in nlp('I can\'t do this')]
Out[29]: [(I, 'PRP'), (ca, 'MD'), (n't, 'RB'), (do, 'VB'), (this, 'DT')]

In [30]: spacy.explain('MD')
Out[30]: 'verb, modal auxiliary'

In [31]: spacy.explain('RB')
Out[31]: 'adverb'

'''
