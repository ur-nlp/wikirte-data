#!/usr/bin/env python3

from tqdm import tqdm
import sys
import json
import random
from pathlib import Path
from nltk.tokenize import PunktSentenceTokenizer
from nltk import Tree
from functools import reduce
from progressbar import progressbar
import re

from pycorenlp import StanfordCoreNLP
nlp = StanfordCoreNLP('http://localhost:9000')


def make_dme_entry(**kwargs):
    entry_template = {
        "sentence1": "A person on a horse jumps over a broken down airplane.",
        "sentence2": "A person is training his horse for a competition.",
        "label": "neutral"
    }

    for kw, val in kwargs.items():
        entry_template[kw] = val

    return entry_template


def entry_to_dme_format(entry, label):
    '''
    converts wikiNLI db entry with label 'label' to SNLI format
    and uses appropriate corresponding fields.
    label must be one of '1' (positive) and '0' (negative)
    '''

    snli_entries = []

    ev_file = entry['evidence']
    try:
        with open('./refdata/wiki-refdata/%s'%ev_file, 'r') as f:
            evid = f.read().strip()
            evid = ''.join(map(lambda char: ' ' if char in '\n\t\r' else char, evid.strip()))
    except FileNotFoundError:
        raise

    spans = [*PunktSentenceTokenizer().span_tokenize(evid)]
    random.shuffle(spans)
    spans = spans[:32] # DEBUG

    numproc = 0
    #print('processing all sentences for entry %d'%entry['claim_id'])
    for i, span in enumerate(spans):
        # print('span:\t', span)
        sent = evid[slice(*span)]
        snli_entry = make_dme_entry(
                sentence2 = entry['claim'],
                sentence1 = sent,
                label = label
            )

        snli_entries.append(snli_entry)
        numproc += 1
    
    return snli_entries


dbpath = Path('./out2/db_2_pos-neg.json')
db = json.loads(dbpath.read_text())

pos, neg = db['positive'], db['negative']
pos, neg = pos[:1000], neg[:1000] # DEBUG

labels = {
            0: 'neutral',
            1: 'entailment',
            'neutral': 0,
            'entailment': 1
        }

props = dict(train=.3, valid=.3, test=.3)


train, valid, test = [], [], []
for label in (0, 1):
    entries = []
    print('processing entries for label %s'%labels[label])
    for entry in tqdm((neg, pos)[label], desc=labels[label]):
        try:
            entries += entry_to_dme_format(entry, labels[label])
        except FileNotFoundError:
            print('couldn\'t find %s'%entry['evidence'], file=sys.stderr)
            continue
        except TypeError:
            # print('error parsing some sentence for %s'%entry['claim'], file=sys.stderr)
            continue
        except UnicodeDecodeError:
            continue
        # break

    print('INFO: total entries collected for label={}: {}'.format(label, len(entries)))
    
    random.shuffle(entries)
    n = len(entries)

    a, b = 0, int(n*props['train'])
    train += entries[slice(a, b)]
    a, b = b, b + int(n*props['valid'])
    valid += entries[slice(a, b)]
    test += entries[b:]


tasks = ['train', 'valid', 'test']
lists = [train, valid, test]
for i in range(3):
    random.shuffle(lists[i])
    with open('./out2/pos-neg/db_2_pos-neg_{}.jsonl'.format(tasks[i]), 'w') as f:
        for entry in lists[i]:
            f.write(json.dumps(entry) + '\n')
