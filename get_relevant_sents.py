#!/usr/bin/env python3

import json
import argparse
import pickle
from pathlib import Path

def get_relevant(entry, common=4):
    '''
    '''

    with open('refdata/wiki-refdata/' + entry['evidence'], 'r', encoding='unicode-escape') as f:
        evtxt = f.read()

    evsents = evtxt.split('.')
    words_in_claim = {w for w in entry['claim'].split() if len(w)>3}

    relevant = [line for line in evsents if len(set(line.split()).intersection(words_in_claim))>=common]

    return relevant



if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('id', type=int, help='pass this entry as input', default=None,
                        nargs='*')
    parser.add_argument('-c', '--common', type=int, help='number of words that need to be in common',
                        default=4)
    args = parser.parse_args()
    print(args)

    if not args.id:
        entry = {
            "context": "Melanistic coyotes owe their black pelts to a mutation that first arose in domestic dogs. A population of nonalbino white coyotes in Newfoundland owe their coloration to a melanocortin 1 receptor mutation inherited from Golden Retriever s. Coyotes have hybridized with wolves to varying degrees, particularly in the Eastern United States and Canada. The so-called eastern coyote of northeastern North America probably originated in the aftermath of the extermination of gray and eastern wolves in the northeast, thus allowing coyotes to colonize former wolf ranges and mix with remnant wolf populations. This hybrid is smaller than either the gray or eastern wolf, and holds smaller territories, but is in turn larger and holds more extensive home ranges than the typical western coyote.",
        "claim": "As of 2010, the eastern coyote's genetic makeup is fairly uniform, with minimal influence from eastern wolves or western coyotes.",
            "evidence": "98a5afc7398769c93586ee85c51348f9.txt",
            "url": "http://www.easterncoyoteresearch.com/downloads/GeneticsOfEasternCoywolfFinalInPrint.pdf",
            "wikipageid": 6710,
            "id": 0,
        }
    else:
        dbpickle = Path('./out2/db_2_prime.pickle')
        if dbpickle.exists():
            with dbpickle.open('rb') as fp:
                db = pickle.load(fp)
        else:
            with open('./out2/db_2_prime.json') as fp, dbpickle.open('wb') as out:
                db = json.load(fp)['sentences']
                pickle.dump(db, out) 
        entry_requested = None
        for entry in db:
            if int(entry['id']) == int(args.id[0]):
                entry_requested = entry
                break
        else:
            raise ValueError('not found')
        entry = entry_requested

    print('claim: ', repr(entry['claim']), end='\n')
    print('sentences in evidence file that may be relevant:')
    for i, sent in enumerate(get_relevant(entry, args.common)):
        print(str(i) + ' ' + '='*32, repr(sent), sep='\n')
