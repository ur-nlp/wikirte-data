% Method section for the wikiRTE dataset paper
% mainfile: `wikirte-data.tex`
% usage: (steps 2-4 only if a citation or bib entry is added/modified)
%   1. `make xelatex FILE=wikirte-data`
%   2. `make bibtex  FILE=wikirte-data`
%   3. `make xelatex FILE=wikirte-data`
%   4. `make xelatex FILE=wikirte-data`

%------------------------------------------------------------------------------
%       BEGIN METHOD SECTION
%------------------------------------------------------------------------------
\section{Method}

%------------------------------------------------------------------------------
%       Source of data
\subsection{Source of data}
Wikipedia makes all of its articles available to download as a data dump on a
biweekly to monthly basis [CITE].
% CITE --> https://dumps.wikimedia.org/enwiki/latest/ ...?
The dump is provided as a collection of XML \textit{pages} each corresponding
to an article or wiki page on Wikipedia. The dump has a compressed size of
approximately 15 gigabytes, and expands to upwards of 60 gigabytes raw data.
We obtained one such recent\footnote{Downloaded in 2018} data dump in English.
For RTE tasks, a relatively recent dump works well as long as claim-evidence
pairs are properly extracted; incremental edits or new content additions are
not as important.

The pages on Wikipedia are written in a markup language called `wikitext', as
specified by MediaWiki, a widely-used wiki markup software used by most of the
Wikimedia Foundation's projects and many other organizations [CITE].
% CITE --> https://www.mediawiki.org/wiki/Manual:What_is_MediaWiki%3F
Wikitext uses fairly straightforward markup, including simplified syntax to
indicate typefaces, define headings and their levels, create paragraphs,
tables, and lists, embed media, and define \textit{templates}, which are
special wikitext objects to encode certain information such as
references/citations, \textit{Wikilinks} (internal links to other pages within
the same wiki instance), etc.

Wikipedia pages in the data dump are individual XML files stringed together,
with certain attributes associated with each page. These include the following.
\begin{itemize}
    \item[\textbf{id}:]     a unique number corresponding to each page
    \item[\textbf{rev}:]    most recent revision id
    \item[\textbf{title}:]  the title or topic of the page
    \item[\textbf{text}:]   the raw wikicode of the page
\end{itemize}


%------------------------------------------------------------------------------
%       Extraction of claims
\subsection{Extraction of claims}

We use a particular wikitext parser called {\ttfamily mwparserfromhell}
(`{\ttfamily mwp}') [CITE].
% CITE --> mwparserfromhell.
The parser calls the raw markup {\itshape wikicode}, and the
corresponding content or plaintext, {\itshape text}. We will stick to these
terms in our description.

%       Algorithm
\subsubsection{Basic algorithm}
We process the wikicode of each of the articles in the dump in a sequential
manner. The goal is to identify sentences that have an associated in-text
citation in the form of a superscript number within square braces. A typical
Wikipedia citation looks like so (taken from `wiki:Albedo'  [CITE]):
% CITE --> wiki article on `albedo'

\begin{quotation}
    \sffamily
    The proportion reflected is not only determined by properties of the
    surface itself, but also by the spectral and angular distribution of solar
    radiation reaching the Earth's surface.\textsuperscript{[$x$]}
    \vskip1em
    [$x$]: \url{http://curry.eas.gatech.edu/Courses/6140/ency/Chapter9/Ency_Atmos/Reflectance_Albedo_Surface.pdf}
\end{quotation}

Wikicode has a notion of {\itshape templates} that mark special wikicode
objects. Wikicode has a template for citations that is marked by the keyword
`Cite'. A citation in wikicode source is embedded in \texttt{ref} tags,
and looks like so:

% if overflows, change url to fol. with an added space for sensible wrap break
% http://curry.eas.gatech.edu/Courses/6140/ency/Chapter9/Ency_Atmos/ Reflectance_Albedo_Surface.pdf
\begin{lstlisting}
<ref>
    {{Cite book
        |url=http://curry.eas.gatech.edu/Courses/6140/ency/Chapter9/Ency_Atmos/Reflectance_Albedo_Surface.pdf
        |title=Reflectance and albedo, surface. Encyclopedia of the Atmosphere, JR Holton and JA Curry (eds.)
        |last=Coakley
        |first=J.A.
        |publisher=Academic Press
        |year=2003
        |isbn=
        |location=
        |pages=1914-1923}}
</ref>
\end{lstlisting}

The parser {\ttfamily mwp} has a notion of \textit{nodes} representing wikicode
markup object of particular categories (tags, templates, headings, etc).
Nodes may be nested: a template may be inside a tag, and a citation may be
inside a wikitable, and so on.
An efficient way to pick out the cited sentences, i.e., claims, is to
go through individual nodes parsed by \texttt{mwp}. We accumulate plaintext
stripped of its markup, and track where in the text accumulated a citation
occurs. In a second iteration, we mark sentence breaks using using NLTK's
`punkt' sentence tokenizer [CITE].
% CITE --> NLTK punkt
We then store a pointer to the beginning of a sentence, and go through the list
of stored $<$reference, index$>$ pairs to match references to their
corresponding sentences. If a citation index is after the `start' index of some
sentence and before or at the `end' index of the same sentence, then the
citation likely occurred somewhere in the sentence.

This method works well to pick out straightforward claim-citation pairs.
However, Wikipedia has many varieties of citations, and we must make
modifications to our algorithm to accommodate and properly pick out most of
them accurately.

%       Modifications necessary for unusual data
\subsubsection{Necessary modifications}

% {\bfseries Multiple citations.}
We can have multiple citations for the same sentence. For instance, consider a
sentence with citations of the form $S^{[x][y]}$. In such a case, we treat the
cited sentence instance as two claim-evidence pairs: $(S,x)$ and $(S,y)$.
A simple modification to the existing method lets us include such examples: we
move the sentence pointer forwards only if the current extracted text is
non-empty (the method will mistake the space between two citations as empty
text in case of multiple citations clumped together).

Another issue is that different parts of sentences may be cited with different
sources, for instance, $S_{1\dots j}\,^{[x]}S_{j+1\dots k}\,^{[y]}
S_{k+1\dots}$. To also accommodate cases of this kind, we maintain another
pointer to the last-known `good' citation location, or the beginning of the
sentence, whichever occurs later. We update the pointer only if we found
non-empty text in a particular iteration. Such updating also takes care of the
potential $S_{1\dots j}\,^{[x][y]}S_{j+1\dots k}\,^{[z]}S_{k+1\dots}$ kind of
references, where for $[y]$ we want the same text as that for $[x]$, and for
$[z]$ we want anything after $[y]$, which is the same as anything after $[x]$,
since the actual citation mark is treated as zero-width for our purpose.

Although we describe a way to pick claims and citations from sentences with
citations spread over the sentence, they are not guaranteed to be well-formed
clauses. Humans add these annotations and are forced to judge by themselves
where a citation must go. For instance, here is one entry from the Wikipedia
article `Albedo' [CITE]:

\begin{quote}
    \sffamily
    Many small objects in the outer Solar System$^{[18]}$ and asteroid belt
    have low albedos down to about 0.05.$^{[19]}$
\end{quote}

Unfortunately, these two citations are not consistently placed. The claim
before citation `$[18]$' doesn't tell us anything, and should instead have been
at the end of the sentence as well, consistent with the way `$[19]$' is placed
at the end of a complete claim. We therefore consider if a claim is well-formed
by checking if there's a verb in the claim-text using Spacy's part-of-speech
tagger [CITE]. If a sentence with multiple citations has a citation not at the
end of a clause, we assume it is grouped together with the next claim-text that
has a verb in the same sentence. If there is no more well-formed claim-text,
we disregard that entry.

%       Exclude entries that are not well-formed
\subsubsection{Exclusion}
We don't pick all of the text-citation instances. Not all are well-formed as we
want them to be, and some are tedious to process.
We exclude any entry with a special character in plaintext that does not have
a straightforward ASCII encoding.
Wikipedia articles can have tables. These are usually marked by the `wikitable'
template. We exclude tables since there is not an obvious way to interpret the
data from tables as a sentence.
Wikipedia articles can have lists. Sometimes each item within a list may have
its own citation. In such cases, one approach could be to string the items
together into one long comma-separated sentence. However, in general, this does
not yield well-formed claims, so we exclude lists. However, there is not a
unified way to tell lists apart. So we look for claims with an unusually high
number of citations, if any, and exclude them.
%It seems that 4 works as a good
%threshold: some {text} that has more than 4 citations, not all
%of which are together, is a good candidate to be a list. In such a case, if the
%total number of words in text is fewer than $5\cdot\#citations$, we assume it's
%a list and exclude the entry.

%------------------------------------------------------------------------------
%       END METHOD SECTION
%------------------------------------------------------------------------------
